# Tools, Configurations, and Packages #

## Visual Studio ##
Microsoft Visual Studio Community 2017 
Version 15.9.6
VisualStudio.15.Release/15.9.6+28307.344
Microsoft .NET Framework
Version 4.7.03056

Installed Version: Community

### Bootstrap ###
v4.2.1

### Entity Framework ###
v6.2.0

### jQuery ###
v3.3.1

### jQuery Validation ###
v1.17.0

### Microsoft.AspNet.Identity.Core ###
v2.2.2

### Microsoft.AspNet.Identity.EntityFramework ###
v2.2.2

### Microsoft.AspNet.Identity.Core ###
v2.2.2

### Microsoft.AspNet.Mvc ###
v5.2.7

### Microsoft.AspNet.Razor ###
v3.2.7

### Newtonsoft.Json ###
v12.0.1

### Microsoft Visual C++ ###
00369-60000-00001-AA646
Visual C++ 2017  

### ASP.NET and Web Tools ###
15.9.04012.0
ASP.NET and Web Tools 2017   

### ASP.NET Core Razor ###
15.8.31590
ASP.NET Core Razor Language Services   

### ASP.NET Web Frameworks and Tools ###
5.2.60913.0
ASP.NET Web Frameworks and Tools 2017   

### Azure App Service Tools ###
15.9.03024.0
Azure App Service Tools v3.0.0   

### Azure Functions and Web Jobs Tools ###
15.9.02046.0
Azure Functions and Web Jobs Tools   

### JavaScript Language Service ###
2.0
JavaScript Language Service

### JavaScript Project System ###
2.0
JavaScript Project System

### JavaScript UWP Project System ###
2.0
JavaScript UWP Project System

### Microsoft Azure Tools ###
Microsoft Azure Tools   2.9
Microsoft Azure Tools for Microsoft Visual Studio 2017 - v2.9.10730.2

### Microsoft Continuous Delivery Tools for Visual Studio ###
0.4
Simplifying the configuration of Azure DevOps pipelines from within the Visual Studio IDE.

### Microsoft Visual Studio Tools for Containers ###
1.1
Develop, run, validate your ASP.NET Core applications in the target environment. F5 your application directly into a container with debugging, or CTRL + F5 to edit & refresh your app without having to rebuild the container.


### Node.js Tools ###
1.4.21001.1 Commit Hash:8dd15923800d931b153ab9e4de74e42a74eba5e6
Adds support for developing and debugging Node.js apps in Visual Studio

### NuGet Package Manager ###
4.6.0
NuGet Package Manager in Visual Studio. For more information about NuGet, visit http://docs.nuget.org/.

### SQL Server Data Tools ###
15.1.61901.03220
Microsoft SQL Server Data Tools

### Visual Basic Tools ###
2.10.0-beta2-63501-03+b9fb1610c87cccc8ceb74a770dba261a58e39c4a
Visual Basic components used in the IDE. Depending on your project type and settings, a different version of the compiler may be used.

### Visual F# ###
Visual F# Tools 10.2 for F# 4.5   15.8.0.0.  Commit Hash: 6e26c5bacc8c4201e962f5bdde0a177f82f88691.
Microsoft Visual F# Tools 10.2 for F# 4.5

### Visual Studio Code Debug Adapter Host Package ###
1.0
Interop layer for hosting Visual Studio Code debug adapters in Visual Studio
