Sprint 3 Planning Sheet : SpockMonkeys
=======================

## As an admin, I would like special privileges ( a different user type) so that I can make changes that normal users can't make.
**Runnan Li Developer**, **8 Effort Points**

### Description
This User Story is aimed at creating a new type of user that will have all of the functionality of a regular user and more. This does not mean that the Admin accounts will necessarily have those privileges in this sprint; it simply means that the separate user account type is available. As an administrator, I would like to have a special type of account that has all the functionality of a regular account but also allows me to make changes so that I can do the things that need to be done without the risk of a normal user screwing things up. User's role is administrator, and there is an 'admin' showed on the page. Admin user has more rights than normal user.

### Acceptance Criteria
_If I log into my admin account, I would like to have access to add or delete non-admin users._

---

## As a user I would like a page where I can update my user info so that I can update my preferences and/or contact info.
**Runnan Li Developer**, **21 Effort Points**

### Description
Users can see the link about user account page. This page will be reachable from the nav bar.
The link will direct user to account information page.
User can edit their information. And the information will be updated in database as well. 
Information include but not limited: password, email, phone number.

### Acceptance Criteria
_If I click on "User account" link through nav bar, I would like to see a page with all my account information.
If I am in User account page, I would like to have access to change my account password.
If I am in User account page, I would like to have access to change my email.
If I am in User account page, I would like to have access to change my phone number._

---

## As a user I would like a place where I can send an email to the developers so that I can tell them about my suggestions/concerns from the web app directly.
**Yumin Chen Developer**, **13 Effort Points**

### Description
Add a button at the bottom of the about page that either pops up a window or redirects to another page where the user can type an email. This box/page will contain a field for the user to type a title, a text area where they can type a message, and a send button below the text area. When they click a send button it will send the message they have typed as an email with the title they have given to our group email.

### Acceptance Criteria
_If I am on the about page, then I am able to send an email to the developers_

---

## As a logged-in user I would like to know when sensor data reaches a certain threshold so that I can take action to correct it.
**Katie Rule Developer**, **13 Effort Points**

### Description
For each device/measurement, we will need to figure out the threshold at which we will want to contact users with an account. We will also need to know how long a device needs to be at that threshold before action is taken. To provide business functionality, we will put an alert system into the web app itself that will pop up an alert on the screen once the threshold has been met.

### Acceptance Criteria
_If the sensor reaches its threshold, then an alert pops up on the dashboard._

---

## As a user, I would like the ability to view past data without leaving the dashboard so that I can see the trends over a period of time in a simple, non-confusing way.
**Jerika Hammett Developer**, **8 Effort Points**

### Description
For each sensor block on the dashboard page, they will have a button on the bottom to expand the section. In the expanded section, we will put a line graph with the past month of data, each point on the line graph being the first measurement taken per day of that month.

### Acceptance Criteria
_If I click on the “Past Data” button on the dashboard, then I am able to see a line graph with the past month of data in the expanded section._

---

## As a user, I would like the ability to display the data in multiple ways, like numerically, graphically or in a speedometer-like gauge, so that I can see it in the way that best makes sense for me.
**Jerika Hammett Developer**, **13 Effort Points**

### Description
In each block, there should be a drop down menu below the visual display of data. The options in the dropdown should be the different methods of display: line graph, scatterplot, gauge, and numerical. When the user selects a different option from the dropdown menu, it should update the display to that type without refreshing the page.

### Acceptance Criteria
_If I am on the dashboard, then there is a drop down menu for each sensor block.
If I select another item in the dropdown menu, then the display updates to that display type.
_

---

## As a user I would like a great looking page to view the current weather in Fishhawk Lake so that I can see the weather there and be happy that the page looks pretty.
**Runnan Li Developer**, **8 Effort Points**

### Description
Update the HTML and CSS in the View so the page looks more pleasing.
The page show the closest city's weather of Fishhawk lake.

### Acceptance Criteria
_If I click on the weather link from home page, I would love to see a weather page that well organized with the display of weather from the closest(available) city near Fishhawk lake._

---

## As a user I would like a speedometer like gauge to view sensor data on tha looks pleasing so that I can see the data in a pleasing way.
**Yumin Chen Developer**, **5 Effort Points**

### Description
Update the Css for the gauge so that it looks more pleasing.

### Acceptance Criteria
_If I am looking at the gauge display, then it I am pleased with the way it looks._

---

## As a user, I would like to have ability to display data in real-time so that I can make sure I'm always seeing the most current data.
**Jerika Hammett Developer**, **13 Effort Points**

### Description
In addition to pulling data from the database there needs to be javascript code that polls the database at an interval (probably either every minute or every five minutes) and updates the display with the new data. In that vein, the displays on the dashboard need to have the most current data.

### Acceptance Criteria
_If I am looking at a display on the dashboard, then it has the most current data.
If new data comes in, then the displays on the dashboard update themselves_

---

## As a user of the system I would like to have a properly unit tested feature
**Jerika Hammett Developer**, **5 Effort Points**

### Description
Learn Unit testing with NUnit

### Acceptance Criteria
_If I have properly learned unit testing with NUnit, then there will be one correctly tested PBI._

---

## As a user of the system I would like to have a properly unit tested feature
**Katie Rule Developer**, **5 Effort Points**

### Description
Learn Unit testing with NUnit

### Acceptance Criteria
_If I have properly learned unit testing with NUnit, then there will be one correctly tested PBI._

---

## As a user of the system I would like to have a properly unit tested feature
**Runnan Li Developer**, **5 Effort Points**

### Description
Learn Unit testing with NUnit

### Acceptance Criteria
_If I have properly learned unit testing with NUnit, then there will be one correctly tested PBI._

---

## As a user of the system I would like to have a properly unit tested feature
**Yumin Chen Developer**, **5 Effort Points**

### Description
Learn Unit testing with NUnit

### Acceptance Criteria
_If I have properly learned unit testing with NUnit, then there will be one correctly tested PBI._

---

## As a user I would like to receive an email when a sensor is at its threshold for more than an hour so that I can take appropriate measures to correct the problem.
**Yumin Chen Developer**, **8 Effort Points**

### Description
We will first need to make sure that we have the API figured out for sending emails. Once we have the API connected to our web app, we will need to set things so that once our thresholds that we have set have been exceeded for one hour or more, it will send an email to the users with accounts every half an hour until the measurements are back at acceptable levels.

### Acceptance Criteria
_If I am a user, the I will receive an email when the threshold has been exceeded for more than an hour_

---

## As a user I would like to receive a text when a sensor is at its threshold for more than an hour so that I can take appropriate measures to correct the problem.
**Yumin Chen Developer**, **8 Effort Points**

### Description
We will first need to make sure that we have the API figured out for sending texts. Once we have the API connected to our web app, we will need to set things so that once our thresholds that we have set have been exceeded for one hour or more, it will send a text to the users with accounts every half an hour until the measurements are back at acceptable levels.

### Acceptance Criteria
_If I am a user, the I will receive a text when the threshold has been exceeded for more than an hour_

---

## As a user I would like to receive a phone call when a sensor is at its threshold for more than an hour so that I can take appropriate measures to correct the problem.
**Yumin Chen Developer**, **8 Effort Points**

### Description
We will first need to make sure that we have the API figured out for sending phone calls. Once we have the API connected to our web app, we will need to set things so that once our thresholds that we have set have been exceeded for one hour or more, it will send a phone call to the users with accounts every half an hour until the measurements are back at acceptable levels.

### Acceptance Criteria
_If I am a user, the I will receive a phone call when the threshold has been exceeded for more than an hour_

---

## As a user I would like to have sensor information available for the web app so that I can see it.
**Katie Rule Developer**, **13 Effort Points**

### Description
We need to have the sensor data accessible to the web app. This involves converting the project from an MVC to an ASP API. Once this is done we can attach our API to our project.

### Acceptance Criteria
_If I am using the web app, then I have access to data from the sensors._

---

## As a user, I would like to have sensor information stored somewhere where I can access it so that I can see past data.
**Katie Rule Developer**, **13 Effort Points**

### Description
Once we have a connection to the project, we need to make sure the sensor data is being stored in the database.

### Acceptance Criteria
_If I am on the dashboard, then I can see data from the database displayed._

---

##  As a user I would like for sensor data to be displayed on the dashboard so that I can see the current data visually.
**Jerika Hammett Developer**, **8 Effort Points**

### Description
When we have data in the database, we will make a sensor display on the dashboard for each device and connect it to the database. This way we will have the most current data displayed for each sensor.

### Acceptance Criteria
_If I am on the dashboard, then I can see sensor data displayed visually._

---

