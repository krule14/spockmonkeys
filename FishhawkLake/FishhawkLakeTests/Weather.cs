﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FishhawkLake.Controllers.HelperClasses;

namespace FishhawkLakeTests
{

    [TestClass]
    public class WeatherTests
    {

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestMethod]
        public void GetDefaultWeatherMap_NotNull()
        {
            Assert.IsNotNull( WeatherControllerHelper.GetDefaultWeatherMap());
        }

        [TestMethod]
        public void GetLongviewResponseWeather_NotNull()
        {
            Assert.IsNotNull(WeatherControllerHelper.GetLongviewResponseWeather());
        }
        [TestMethod]
        public void GetWeatherList_NotNull()
        {
            Assert.IsNotNull(WeatherControllerHelper.GetWeatherList());
        }
        [TestMethod]
        public void GetWeatherGui_NotNull()
        {
            Assert.IsNotNull(WeatherControllerHelper.GetWeatherGui());
        }

        
    }
}
