﻿using System;
using FishhawkLake.Controllers;
using System.Web;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace FishhawkLakeTests
{
    [TestClass]
    public class JerikasUnitTest
    {
        [TestMethod]
        public void InputIsIncremented_ReturnsTrue()
        {
            //arrange
            int x = 1;

            //act
            HomeController home = new HomeController();

            //assert
            var result = home.Incrementer(x);
            Assert.AreEqual(x + 1, result);
        }

        [TestMethod]
        public void InputIsIncremented_ReturnsFalse()
        {
            //arrange
            int x = 1;

            //act
            HomeController home = new HomeController();

            //assert
            var result = home.Incrementer(x);
            Assert.AreNotEqual(x, result);
        }
    }
}
