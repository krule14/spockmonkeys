﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FishhawkLake.Controllers.HelperClasses;

namespace FishhawkLakeTests
{
    [TestClass]
    public class LinearRegressionTest
    {
        [TestMethod]
        public void Fresh_CalibrationHelper_HasAllZeroVariables()
        {
            CalibrationHelper myCalibration = new CalibrationHelper();
            Assert.IsTrue(myCalibration.SumOfRawValues == 0);
            Assert.IsTrue(myCalibration.SumOfDesiredValues == 0);
            Assert.IsTrue(myCalibration.SumOfSquaredRawValues == 0);
            Assert.IsTrue(myCalibration.SumOfRawAndDesiredValues == 0);
            Assert.IsTrue(myCalibration.NumberOfEntriesForCalibration == 0);
            Assert.IsTrue(myCalibration.CurrentSlope == 0);
            Assert.IsTrue(myCalibration.CurrentYIntercept == 0);
        }
        [TestMethod]
        public void XAndFunction_GivesCorrectOutcome()
        {
            CalibrationHelper myCalibration = new CalibrationHelper();
            //data * 0.0304 - 12.313; //This is a psi calibration
            myCalibration.SetSlope(0.0304);
            myCalibration.SetIntercept(-12.313);

            double result = myCalibration.ConvertRawValToDesired(2872.00);
            result= Math.Round(result);
            //the output should be something like 74.9958, but computers can be different, so I'm rounding. 

            double result2 = myCalibration.ConvertRawValToDesired(2773.00);
            result2 = Math.Round(result2);
            //the output should be something like 71.982, but computers can be different, so I'm rounding. 



            Assert.IsTrue(result == 75.00);
            Assert.IsTrue(result2 == 72.00);
        }

    }
}
