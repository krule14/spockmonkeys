namespace FishhawkLake.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Calibration
    {
        [Key]
        public int CID { get; set; }

        [Required]
        [StringLength(64)]
        public string DeviceID { get; set; }

        public double? Slope { get; set; }

        public double? Intercept { get; set; }

        public double? X { get; set; }

        public double? Y { get; set; }
    }
}
