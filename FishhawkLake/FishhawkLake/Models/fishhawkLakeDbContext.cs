namespace FishhawkLake.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class FishhawkLakeDbContext : DbContext
    {
        public FishhawkLakeDbContext()
        //: base("name=FishhawkLakeDbContext")
        //: base("LakeDbContext")
        : base("fishhawkLakeDbContext")
        {

        }

        public virtual DbSet<Device> Devices { get; set; }
        public virtual DbSet<Alert> Alerts { get; set; }
        public virtual DbSet<AspNetUser> AspNetUsers { get; set; }
        //public AspNetUser AspNetUser {get; set;}
        //public AspNetRole AspNetRole { get; set; }
        //public virtual DbSet<AspNetUserRoles> AspNetUserRoles { get; set; }
        //public virtual DbSet<AspNetRole> AspNetRoles { get; set; }
        public virtual DbSet<AlertUserContact> AlertUserContact { get; set; }
        public virtual DbSet<PostReqError> PostReqError { get; set; }
        public virtual DbSet<Threshold> Threshold { get; set; }

        public virtual DbSet<Calibration> Calibrations { get; set; }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            Database.SetInitializer<FishhawkLakeDbContext>(null);
            base.OnModelCreating(modelBuilder);
        }
    }
}
