﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FishhawkLake.Models
{
    public class AlertSelectionModel
    {
        //Alert is on 
        public Boolean AlertOn { get; set; }
        //Contact person's email
        public Boolean email { get; set; }
        //Contact person's subject
        public Boolean text { get; set; }
        //Contact person's message
        public Boolean phone { get; set; }

    }
}