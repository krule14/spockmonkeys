namespace FishhawkLake.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Alert
    {
        public int ID { get; set; }

        public DateTime Time { get; set; }

        public double Value { get; set; }

        [Required]
        [StringLength(64)]
        public string DeviceID { get; set; }
    }
}
