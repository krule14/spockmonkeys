﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FishhawkLake.Models.ViewModel
{
    public class AlertLogViewModel
    {
        public IEnumerable<Alert> Alerts { get; set; }
        public PagingInfo PagingInfo { get; set; }
        public string DID { get; set; }

        
        public string StartDate { get; set; }
        public string EndDate { get; set; }
    }
}