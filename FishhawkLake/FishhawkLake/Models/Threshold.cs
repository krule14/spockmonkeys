namespace FishhawkLake.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Threshold")]
    public partial class Threshold
    {
        public int ID { get; set; }

        [Required]
        [StringLength(64)]
        public string DeviceID { get; set; }

        [Column("Threshold")]
        public double Threshold1 { get; set; }

    }
}
