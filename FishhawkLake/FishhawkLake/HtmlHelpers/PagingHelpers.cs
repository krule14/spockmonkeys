﻿using FishhawkLake.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace FishhawkLake.HtmlHelpers
{
    public static class PagingHelpers
    {
        public static MvcHtmlString PageLinks(this HtmlHelper html, PagingInfo pagingInfo, Func<int, string> pageUrl)
        {
            StringBuilder result = new StringBuilder();

            TagBuilder tag = new TagBuilder("a");
            TagBuilder first = new TagBuilder("a");
            TagBuilder last = new TagBuilder("a");
            TagBuilder previous = new TagBuilder("a");
            TagBuilder next = new TagBuilder("a");
                       
            first.MergeAttribute("href", pageUrl(pagingInfo.FirstPage));
            first.InnerHtml = "First";
            first.AddCssClass("btn btn-default");
            result.Append(first.ToString());

            if (pagingInfo.CurrentPage > 1)
            {
                previous.MergeAttribute("href", pageUrl(pagingInfo.CurrentPage - 1));
                previous.InnerHtml = "Previous";
                previous.AddCssClass("btn btn-default");
                result.Append(previous.ToString());
            }
           
            for (int i = pagingInfo.CurrentPage - 2; i <= pagingInfo.CurrentPage + 2; i++)
            {
                if(i > 0 && i <= pagingInfo.TotalPages)
                {
                    tag.MergeAttribute("href", pageUrl(i));
                    tag.InnerHtml = i.ToString();
                    if (i == pagingInfo.CurrentPage)
                    {
                        tag.AddCssClass("selected");
                        tag.AddCssClass("btn-primary");
                    }
                    tag.AddCssClass("btn btn-default");
                    result.Append(tag.ToString());
                }
                
            }

            if (pagingInfo.CurrentPage < pagingInfo.TotalPages)
            {
                next.MergeAttribute("href", pageUrl(pagingInfo.CurrentPage + 1));
                next.InnerHtml = "Next";
                next.AddCssClass("btn btn-default");
                result.Append(next.ToString());
            }

            last.MergeAttribute("href", pageUrl(pagingInfo.LastPage));
            last.InnerHtml = "Last";
            last.AddCssClass("btn btn-default");
            result.Append(last.ToString());

            return MvcHtmlString.Create(result.ToString());
        }
    }
}