﻿using FishhawkLake.Models;
using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.IO;
using FishhawkLake.Class;
using System.Text;
using System.Data;
using Newtonsoft.Json;
using System.Net.Mail;
using System.Web.UI;
using FishhawkLake.Controllers.HelperClasses;

namespace FishhawkLake.Controllers
{
    public class HomeController : Controller
    {
        private FishhawkLakeDbContext db = new FishhawkLakeDbContext();
        public ActionResult Index()
        {
            return View();
        }

        public int Incrementer(int a)
        {
            int b = a + 1;
            return b;
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult LineGraph()
        {
            return View();
        }

        
        [HttpGet]
        public JsonResult DrawLineGraph(string devID, int dateRange = 1)
        {
            DateTime start = DateTime.Now.AddDays(-dateRange);
            var data = db.Devices.OrderByDescending(x => x.ID).
                Where(x => x.DeviceID == devID).
                Where(m => m.Time.Day >= start.Day).
                OrderBy(x => x.Time).ToList();
            return Json(data, JsonRequestBehavior.AllowGet);
        }



        public ActionResult ScatterPlot()
        {
            return View();
        }

        public ActionResult Gauge()
        {
            return View();
        }

        public ActionResult LiveDataDisplay()
        {
            return View();
        }

        public ActionResult Device()
        {
            var list = db.Devices.OrderByDescending(x => x.Time).ToList().Take(10);
            return View(list);
        }

        public ActionResult Turbidity()
        {
            var list = db.Devices.Where(v => v.DeviceID == "pubTurbidity").OrderByDescending(x => x.Time).ToList().Take(10);
            return View(list);
        }

        public ActionResult Tank()
        {
            var list = db.Devices.Where(v => v.DeviceID == "pubPsi").OrderByDescending(x => x.Time).ToList().Take(10);
            return View(list);
        }

        [Authorize]
        public ActionResult Dashboard()
        {
            ViewBag.Display = "none";

            return View();
        }

        

        
        public ActionResult ContactUs()
        {
            return View();
        }

        [HttpPost]
        public ActionResult ContactUs(ContactModel contact)
        {
            string email = contact.email;
            string subject = contact.subject;
            string message = contact.message;

            SmtpClient mail = new SmtpClient("smtp.gmail.com");
            mail.Port = 587;
            mail.EnableSsl = true;
            mail.Credentials = new NetworkCredential("alarttestmail123@gmail.com", "ZAQ!2wsx");

            MailMessage mailmessage = new MailMessage();
            mailmessage.From = new MailAddress("alarttestmail123@gmail.com");
            mailmessage.To.Add("fishhawklakewaterplant@gmail.com");
            mailmessage.Subject = "FromEmail: "+email+" + "+subject;
            mailmessage.Body = message;
            mail.Send(mailmessage);

            ViewBag.result = "Email Send to Support Team";
            return View();
        }
        
       /*
            public OpenWeatherMap Salem()
            {
                OpenWeatherMap openWeatherMap = new OpenWeatherMap();
                openWeatherMap.cities = new Dictionary<string, string>();

                openWeatherMap.cities.Add("Fishhawk Lake", "4784205");
                //openWeatherMap.cities.Add("Portland", "4720131");
                //openWeatherMap.cities.Add("Woodburn", "5761708");
                //openWeatherMap.cities.Add("Jewell", "4862659");
                //openWeatherMap.cities.Add("Woodland", "4137001");
                return openWeatherMap;
            }
            */

        

    }
}