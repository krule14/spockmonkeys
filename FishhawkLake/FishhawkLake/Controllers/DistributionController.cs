﻿ using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FishhawkLake.Models;


namespace FishhawkLake.Controllers
{
    public class DistributionController : Controller
    {
        private FishhawkLakeDbContext db = new FishhawkLakeDbContext();

        //Return latest PSI from DB
        public double getPSI()
        {
            var psi = db.Devices.Where(v => v.DeviceID == "pubpsi").Select(s => s.Value).ToArray().Last();

            return psi;
        }

        /*
        public double getWL()
        {
            var water = db.Devices.Where(v => v.DeviceID == "waterlevel").Select(s => s.Value).ToArray().Last();

            return water;
           
        }
        */

        public JsonResult getPsiJSON()
        {
            var psi = getPSI();
            return Json(psi, JsonRequestBehavior.AllowGet);
        }

        /*
        public JsonResult getWlJSON()
        {
            var wl = getWL();
            return Json(wl, JsonRequestBehavior.AllowGet);

        }
        */

    }
}