﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FishhawkLake.Controllers.HelperClasses;
using FishhawkLake.Models;

namespace FishhawkLake.Controllers
{
    public class CalibrationsController : Controller
    {
        private FishhawkLakeDbContext db = new FishhawkLakeDbContext();
        private static double prevX;
        private static double prevY;

        // GET: Calibrations
        public ActionResult Index()
        {
            return View(db.Calibrations.ToList());
        }

        // GET: Calibrations/NoFunctionError
        public ActionResult NoFunctionsError()
        {
            return View();
        }

        // GET: Calibrations/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Calibrations/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "CID,DeviceID,Slope,Intercept,X,Y")] Calibration calibration)
        {
            if (ModelState.IsValid)
            {
                if(calibration.X !=null)
                {
                    CalibrationHelper helper = new CalibrationHelper();
                    Tuple<double,double,double,double> results= 
                        helper.FillInTheBlank(calibration.Slope, calibration.Intercept, calibration.X, calibration.Y);
                    Calibration newCali = new Calibration();
                    newCali.CID = calibration.CID;
                    newCali.DeviceID = calibration.DeviceID;
                    newCali.Slope = results.Item1;
                    newCali.Intercept = results.Item2;
                    newCali.X = results.Item3;
                    newCali.Y = results.Item4;
                    
                    db.Calibrations.Add(newCali);
                    db.SaveChanges();
                    return RedirectToAction("Index");

                }
                db.Calibrations.Add(calibration);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(calibration);
        }

        // GET: Calibrations/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Calibration calibration = db.Calibrations.Find(id);
            if (calibration == null)
            {
                return HttpNotFound();
            }
            if(calibration.X!=null && calibration.Y != null)
            {
                prevX=(double)calibration.X;
                prevY = (double)calibration.Y;
            }
            return View(calibration);
        }

        // POST: Calibrations/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "CID,DeviceID,Slope,Intercept,X,Y")] Calibration calibration)
        {
            CalibrationHelper helper = new CalibrationHelper();
            if (ModelState.IsValid)
            {
                if (calibration.X != null)
                {
                    
                    if (prevX != 0 && prevY != 0)
                    {
                        helper.AddRawAndDesiredValues(prevX, prevY);
                        if (calibration.X != null && calibration.Y!= null) {
                            helper.AddRawAndDesiredValues((double)calibration.X,(double)calibration.Y);
                         }
                    }
                    // Tuple<double, double, double, double> results =
                    //helper.FillInTheBlank(calibration.Slope, calibration.Intercept, calibration.X, calibration.Y);
                    //calibration.Slope = results.Item1;
                    //calibration.Intercept = results.Item2;
                    //calibration.X = results.Item3;
                    //calibration.Y = results.Item4;
                    double newslope = helper.GetSlope();
                    double newintercept = helper.GetYIntercept();
                    if ( newslope != calibration.Slope && newslope != 0)
                    {
                        calibration.Slope = newslope;
                    }
                    if (newintercept != calibration.Slope && newintercept != 0)
                    {
                        calibration.Intercept = newintercept;
                    }
                    db.Entry(calibration).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                db.Entry(calibration).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(calibration);
        }

        //Add New Datapoint
        // GET: Calibrations/Edit/5
        public ActionResult EditXY(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Calibration calibration = db.Calibrations.Find(id);
            if (calibration == null)
            {
                return HttpNotFound();
            }
            if (calibration.X != null && calibration.Y != null)
            {
                prevX = (double)calibration.X;
                prevY = (double)calibration.Y;
            }
            return View(calibration);
        }
        
        //Add New Datapoint
        // POST: Calibrations/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditXY([Bind(Include = "CID,DeviceID,Slope,Intercept,X,Y")] Calibration calibration)
        {
            CalibrationHelper helper = new CalibrationHelper();
            if (ModelState.IsValid)
            {
                if (calibration.X != null)
                {

                    if (prevX != 0 && prevY != 0)
                    {
                        helper.AddRawAndDesiredValues(prevX, prevY);
                        if (calibration.X != null && calibration.Y != null)
                        {
                            helper.AddRawAndDesiredValues((double)calibration.X, (double)calibration.Y);
                        }
                    }
                    double newslope = helper.GetSlope();
                    double newintercept = helper.GetYIntercept();
                    if (newslope != calibration.Slope && newslope != 0)
                    {
                        calibration.Slope = newslope;
                    }
                    if (newintercept != calibration.Slope && newintercept != 0)
                    {
                        calibration.Intercept = newintercept;
                    }
                    

                    db.Entry(calibration).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                db.Entry(calibration).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(calibration);
        }

        // GET: Calibrations/Edit/5
        public ActionResult EditMB(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Calibration calibration = db.Calibrations.Find(id);
            if (calibration == null)
            {
                return HttpNotFound();
            }
            if (calibration.X != null && calibration.Y != null)
            {
                prevX = (double)calibration.X;
                prevY = (double)calibration.Y;
            }
            return View(calibration);
        }

        // POST: Calibrations/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditMB([Bind(Include = "CID,DeviceID,Slope,Intercept,X,Y")] Calibration calibration)
        {
            CalibrationHelper helper = new CalibrationHelper();
            if (ModelState.IsValid)
            {
                if (calibration.X != null)
                {

                    if (prevX != 0 && prevY != 0)
                    {
                        helper.AddRawAndDesiredValues(prevX, prevY);
                        if (calibration.X != null && calibration.Y != null)
                        {
                            helper.AddRawAndDesiredValues((double)calibration.X, (double)calibration.Y);
                        }
                    }
                    double newslope = helper.GetSlope();
                    double newintercept = helper.GetYIntercept();
                    if (newslope != calibration.Slope && newslope != 0)
                    {
                        calibration.Slope = newslope;
                    }
                    if (newintercept != calibration.Slope && newintercept != 0)
                    {
                        calibration.Intercept = newintercept;
                    }
                    
                    db.Entry(calibration).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                db.Entry(calibration).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(calibration);
        }

        // GET: Calibrations/Edit/5
        public ActionResult CalculateY(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Calibration calibration = db.Calibrations.Find(id);
            if (calibration == null)
            {
                return HttpNotFound();
            }
            if (calibration.Slope == null && calibration.Intercept == null)
            {
                return RedirectToAction("NoFunctionsError");
            }
            return View(calibration);
        }

        // POST: Calibrations/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CalculateY([Bind(Include = "CID,DeviceID,Slope,Intercept,X,Y")] Calibration calibration)
        {
            CalibrationHelper helper = new CalibrationHelper();
            if (ModelState.IsValid)
            {
                if (calibration.X != null && calibration.Slope != null && calibration.Intercept != null)
                {
                    helper.SetSlope((double)calibration.Slope);
                    helper.SetIntercept((double)calibration.Intercept);


                    ViewBag.YValue = helper.ConvertRawValToDesired((double)calibration.X);
                }
               
            }
            return View(calibration);
        }


        // GET: Calibrations/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Calibration calibration = db.Calibrations.Find(id);
            if (calibration == null)
            {
                return HttpNotFound();
            }
            return View(calibration);
        }

        // POST: Calibrations/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Calibration calibration = db.Calibrations.Find(id);
            db.Calibrations.Remove(calibration);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
