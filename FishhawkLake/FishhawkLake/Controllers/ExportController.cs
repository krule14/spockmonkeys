﻿using FishhawkLake.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace FishhawkLake.Controllers
{
    public class ExportController : Controller
    {

        FishhawkLakeDbContext db = new FishhawkLakeDbContext();

        [HttpGet]
        public ActionResult Export()
        {
            List<string> deviceIDs = new List<string>();
            deviceIDs.Add("All");
            deviceIDs.Add("Psi");
            deviceIDs.Add("Turbidity");
            deviceIDs.Add("Water Level");
            deviceIDs.Add("Chlorine");
            ViewBag.deviceIDs = deviceIDs;

            return View();
        }

        [HttpPost]
        public ActionResult Export(FormCollection collection)
        {
            try
            {
                DateTime startDate = Convert.ToDateTime(collection["StartDate"]);
                DateTime endDate = Convert.ToDateTime(collection["EndDate"]);
                IEnumerable<Device> list = db.Devices.OrderByDescending(x => x.Time).ToList();
                string deviceID = "pub" + collection["DID"].ToLower();

                System.Diagnostics.Debug.WriteLine("device: " + deviceID);
                System.Diagnostics.Debug.WriteLine("StartDate: " + collection["StartDate"]);
                System.Diagnostics.Debug.WriteLine("startDate: " + startDate);
                System.Diagnostics.Debug.WriteLine("EndDate: " + collection["EndDate"]);
                System.Diagnostics.Debug.WriteLine("endDate: " + endDate);

                if(collection["Device"] == "All")
                {
                    list = db.Devices.OrderByDescending(x => x.Time)
                        .Where(x => x.Time >= startDate && x.Time <= endDate)
                        .ToList();
                }
                else
                {
                    list = db.Devices.OrderByDescending(x => x.Time)
                        .Where(x => x.DeviceID == deviceID)
                        .Where(x => x.Time >= startDate && x.Time <= endDate)
                        .ToList();
                }
                
                var sb = new StringBuilder();
                sb.AppendFormat("{0}, {1}, {2}, {3}", "Time", "Value", "Device ID", Environment.NewLine);
                foreach (var item in list)
                {
                    sb.AppendFormat("{0}, {1}, {2}, {3}", item.Time, item.Value, item.DeviceID, Environment.NewLine);
                }

                var response = System.Web.HttpContext.Current.Response;
                response.BufferOutput = true;
                response.Clear();
                response.ClearHeaders();
                response.ContentEncoding = Encoding.Unicode;
                response.AddHeader("content-disposition", "attachment;filename=FilteredAlerts.csv");
                response.ContentType = "text/plain";
                response.Write(sb.ToString());
                response.End();

                return RedirectToAction("Export");
            }
            catch
            {
                return View();
            }

            
            
        }

        

        public void ExportAllCSV()
        {
            var sb = new StringBuilder();
            IEnumerable<Alert> list = db.Alerts.OrderBy(x => x.Time).ToList();
            sb.AppendFormat("{0}, {1}, {2}, {3}", "Time", "Value", "Device ID", Environment.NewLine);
            foreach(var item in list)
            {
                sb.AppendFormat("{0}, {1}, {2}, {3}", item.Time, item.Value, item.DeviceID, Environment.NewLine);
            }

            var response = System.Web.HttpContext.Current.Response;
            response.BufferOutput = true;
            response.Clear();
            response.ClearHeaders();
            response.ContentEncoding = Encoding.Unicode;
            response.AddHeader("content-disposition", "attachment;filename=Alerts.csv");
            response.ContentType = "text/plain";
            response.Write(sb.ToString());
            response.End();
        }


    }
}