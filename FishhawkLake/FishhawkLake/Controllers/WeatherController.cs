﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FishhawkLake.Models;
using System.Diagnostics;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.IO;
using FishhawkLake.Class;
using System.Text;
using System.Data;
using Newtonsoft.Json;
using FishhawkLake.Controllers.HelperClasses;

namespace FishhawkLake.Controllers
{
    public class WeatherController : Controller
    {
        //GET
        public ActionResult Weather()
        {
            
            return View(WeatherControllerHelper.GetDefaultWeatherMap());
        }

        //POST
        [HttpPost]
        public ActionResult Weather(OpenWeatherMap openWeatherMap, string cities)
        {
            openWeatherMap = WeatherControllerHelper.GetDefaultWeatherMap();

            if (cities != null)
            {
                openWeatherMap.apiResponse =WeatherControllerHelper.GetWeatherList();
            }
            else
            {
                if (Request.Form["submit"] != null)
                {
                    openWeatherMap.apiResponse = "► Select City";
                }
            }
            return View(openWeatherMap);
        }
    }
}