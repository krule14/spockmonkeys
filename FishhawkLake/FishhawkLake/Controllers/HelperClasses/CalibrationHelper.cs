﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FishhawkLake.Controllers.HelperClasses
{
    public class CalibrationHelper
    {
        public double SumOfRawValues = 0; //x'
        public double SumOfDesiredValues = 0;//y'
        public double SumOfSquaredRawValues = 0;//E(x^2)
        public double SumOfRawAndDesiredValues = 0;//E (xy)
        public double NumberOfEntriesForCalibration = 0;//n
        public double CurrentSlope = 0;// m
        public double CurrentYIntercept = 0;//b

        /*
         * Let m be slope. Let n be the number of entries. 
         * Let x be our raw value (independant variable)
         * Let y be our desired value (dependant variable)
         * Let x' be the sum of all x's
         * Let y' be the sum of all y's
         * Let "E" be our symbol for summation (Sigma)
         */
        public void AddRawAndDesiredValues(double rawValue, double desiredValue)
        {
            
            SumOfRawValues += rawValue;//x'
            SumOfDesiredValues += desiredValue;//y'
            SumOfSquaredRawValues += rawValue * rawValue;//E(x^2)
            SumOfRawAndDesiredValues += rawValue * desiredValue;//E (xy)
            NumberOfEntriesForCalibration++;//n
            CalculateFunct();
        }
        public double ConvertRawValToDesired(double rawVal)
        {
            return rawVal * GetSlope() + GetYIntercept();
        }

        public void CalculateFunct()
        {
            CalculateSlope();
            CalculateIntercept();
        }

        /*
         * Let m be slope. Let n be the number of entries. 
         * Let x be our raw value (independant variable)
         * Let y be our desired value (dependant variable)
         * Let x' be the sum of all x's
         * Let y' be the sum of all y's
         * Let "E" be our symbol for summation (Sigma)
         * 
         * Then our solution for slope is:
         *   n*( E(xy) ) - x'y'
         *   -------------------
         *   n*( E x^2 )-(x')^2
         *   
         */
        private double CalculateSlope() {
            double numerator = 0;
            //n*( E(xy) )
            numerator = NumberOfEntriesForCalibration*SumOfRawAndDesiredValues;
            //n*( E(xy) ) - x'y'
            numerator -= SumOfRawValues * SumOfDesiredValues;

            double demominator = 0;
            // n*(E x ^ 2 )
            demominator += NumberOfEntriesForCalibration * SumOfSquaredRawValues;
            //n*(E x ^ 2 )-(x')^2
            demominator -= SumOfRawValues * SumOfRawValues;

            /*   n*( E(xy) ) - x'y'
             *   -------------------
             *   n*( E x^2 )-(x')^2
             */
            if (demominator != 0)
            {
                CurrentSlope = numerator / demominator;

            }else { CurrentSlope = 0; }

            return CurrentSlope;
        }

        /*
         * Let m be slope. Let n be the number of entries. 
         * Let x be our raw value (independant variable)
         * Let y be our desired value (dependant variable)
         * Let x' be the sum of all x's
         * Let y' be the sum of all y's
         * Let "E" be our symbol for summation (Sigma)
         * 
         * Then our solution for intercept is:
         *   y'*(E (x^2) )- x'*( E(xy) )
         *   ----------------------------
         *      n*( E x^2 )- (x')^2
         *   
         */
        private double CalculateIntercept() {
            double numerator = 0;
            //y'*(E (x^2) )
            numerator = SumOfDesiredValues * SumOfSquaredRawValues;
            //y'*(E (x^2) )- x'*( E(xy) )
            numerator -= SumOfRawValues * SumOfRawAndDesiredValues;

            double demominator = 0;
            //n*( E x^2 )
            demominator = NumberOfEntriesForCalibration * SumOfSquaredRawValues;
            //n * (E x ^ 2 )-(x')^2
            demominator -= SumOfRawValues * SumOfRawValues;

            /*   y'*(E (x^2) )- x'*( E(xy) )
            *   ----------------------------
            *      n*( E x^2 )- (x')^2
            */
            if (demominator != 0)
            {
                CurrentYIntercept = numerator / demominator;

            }
            else { CurrentYIntercept = 0; }

            return CurrentYIntercept;
        }
        public void SetSlope(double slope) { CurrentSlope = slope; }
        public void SetIntercept(double intercept) { CurrentYIntercept = intercept; }
        public double GetSlope() { return CurrentSlope; }
        public double GetYIntercept() { return CurrentYIntercept; }
        public bool HasFunction(double? slope, double? intercept)
        {
            if(slope!=null && intercept != null)
            {
                return true;
            }
            return false;
        }
        public bool HasFunctAndX(double? slope, double? intercept, double? x)
        {
            if(HasFunction(slope, intercept)&& x != null)
            {
                return true;
            }
            return false;
        }
        public bool HasXAndY(double? x, double? y)
        {
            if(x!= null && y!= null)
            {
                return true;
            }
            return false;
        }
        public bool HasAll(double? slope, double? intercept, double? x, double? y)
        {
            if(HasFunction(slope, intercept)&& HasXAndY(x, y))
            {
                return true;
            }
            return false;
        }
        public Tuple<double,double,double,double> FillInTheBlank(double? slope, double? intercept, double? x, double? y)
        {
            if(HasAll(slope, intercept, x, y))
            {
                return Tuple.Create((double)slope, (double)intercept, (double)x, (double)y);
            }
            if (HasFunctAndX(slope, intercept, x))
            { 
                SetSlope((double)slope);
                SetIntercept((double)intercept);
                double myIntercept = ConvertRawValToDesired((double)x);
                return Tuple.Create((double)slope, (double)intercept, (double)x, myIntercept);
            }
            if (HasXAndY(x, y))
            {
                AddRawAndDesiredValues((double)x, (double)y);
                double mySlope = GetSlope();
                double myIntercept = GetYIntercept();
                return Tuple.Create(mySlope, myIntercept, (double)x, (double)y);
            }
            return Tuple.Create(-1.0, -1.0, -1.0, -1.0);

        }
    }
}