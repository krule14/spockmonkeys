﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FishhawkLake.Concrete.Persistance;
using FishhawkLake.Models;

namespace FishhawkLake.Controllers.HelperClasses
{
    public static class AlarmThresholdManager
    {
        //private static Tuple<string, double> LastThresholdTriggered = new Tuple<string, double>("Hasn't Triggered", double.NaN);
        //private static Boolean ThresholdAcknowledged = true;
        private static FishhawkLakeDbContext db = new FishhawkLakeDbContext();
        //private static UnitOfWork unitOfWork = new UnitOfWork(new FishhawkLakeDbContext());


        //private static IDictionary<string, double> Thresholds = new Dictionary<string, double>(){
        //    {"pubPsi", 78},
        //    {"pubpsi", 78},
        //    { "pubTurb", 3.00},//Not real
        //    { "pubChlor", 1.00}//Not real
        //};

        //public static Tuple<string, double> GetLastThresholdTriggered()
        //{
        //    ThresholdAcknowledged = true;
        //    return LastThresholdTriggered;
        //}
        //public static Boolean GetThresholdAcknowledged(){return ThresholdAcknowledged;}

        //private static void SetLastThresholdTriggered(string DeviceID, double Value)
        //{
            //LastThresholdTriggered = Tuple.Create(DeviceID, Value);
        //    Threshold th = (from p in db.Threshold where p.DeviceID == DeviceID select p).SingleOrDefault();
        //    th.Threshold1 = Value;
        //    db.SaveChanges();
        //}

        private static double givenAnIdFindLatestValue(string id, FishhawkLakeDbContext db)
        {
            var latest = db.Devices.Where(v => v.DeviceID == id).Select(s => s.Value).ToArray().Last();
            return (Convert.ToDouble(latest));
        }
        public static Boolean HasPassedThreshold(string DeviceID, double Value)
        {
            //int count = unitOfWork.Threshholds.Thresholds.Where(p => p.DeviceID == DeviceID).Count();
            if (db.Database.Connection.State == ConnectionState.Closed)
            {
                db.Database.Connection.Open();
            }
            int count = db.Threshold.Where(p => p.DeviceID == DeviceID).Count();
            double data = db.Threshold.Where(p => p.DeviceID == DeviceID).Select(s => s.Threshold1).SingleOrDefault();
            if (count > 0 && (Value > data))
            {
                //SetLastThresholdTriggered(DeviceID, Value);
                //ThresholdAcknowledged = false;
                return true;
            }
            return false;
        }

        public static double RetureThreshold(string DeviceID)
        {
            if (true)
            {
                double data = db.Threshold.Where(p => p.DeviceID == DeviceID).Select(s => s.Threshold1).SingleOrDefault();
                return data;
            }
            //return -1;
        }

    }

}